# MUSIC PLAYER APP

<img src = "assets/images/overview_ui.png">
<p>
A Music Player app built using the Flutter framework and [iTunes Open API](https://developer.apple.com/library/archive/documentation/AudioVideo/Conceptual/iTuneSearchAPI/index.html).
</p>

## Description

This application is a song player from the internet. All songs are obtained from iTunes Open API.
<br>
Default music list is a song from Tiara Andini but you can search for any song you want in the search input section. Other features of this application include :
* Search song
* Play song
* Pause & resume song
* Seek songs using slider
* Set repeat mode


## Getting Started

### Flutter Version

* 3.3.10

### Dependencies

* audioplayers
* flutter_bloc
* bloc
* equatable
* dio
* intl

### Intermezzo
<p> For now this project is built on the Android platform. So for the iOS platform I am not sure if it can run well.
<br>
This is because when developing this project I use a Windows device. </p>

### Executing

* flutter pub get
* flutter run
