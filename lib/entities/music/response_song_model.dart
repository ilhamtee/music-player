import 'dart:io';

class ResponseSongModel {
  List<Song>? listSong;
  bool? status;
  int? statusCode;

  ResponseSongModel({this.listSong, this.status = false, this.statusCode});

  ResponseSongModel.fromJson(Map<String, dynamic> json) {
    if (json['results'] != null) {
      listSong = <Song>[];
      json['results'].forEach((v) {
        listSong!.add(Song.fromJson(v));
      });
      status = true;
      statusCode = HttpStatus.ok;
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (listSong != null) {
      data['results'] = listSong!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Song {
  String? kind;
  int? artistId;
  int? trackId;
  String? artistName;
  String? trackName;
  String? collectionName;
  String? previewUrl;
  String? artworkUrl100;
  String? releaseDate;
  int? trackTimeMillis;
  String? country;
  String? primaryGenreName;
  bool? isFavoriteSong;

  Song(
      {this.kind,
      this.artistId,
      this.trackId,
      this.artistName,
      this.trackName,
      this.collectionName,
      this.previewUrl,
      this.artworkUrl100,
      this.releaseDate,
      this.trackTimeMillis,
      this.country,
      this.primaryGenreName,
      this.isFavoriteSong = false});

  Song.fromJson(Map<String, dynamic> json) {
    kind = json['kind'];
    artistId = json['artistId'];
    trackId = json['trackId'];
    trackName = json['trackName'];
    artistName = json['artistName'];
    collectionName = json['collectionName'];
    previewUrl = json['previewUrl'];
    artworkUrl100 = json['artworkUrl100'];
    releaseDate = json['releaseDate'];
    trackTimeMillis = json['trackTimeMillis'];
    country = json['country'];
    primaryGenreName = json['primaryGenreName'];
    isFavoriteSong = false;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['kind'] = kind;
    data['artistId'] = artistId;
    data['trackId'] = trackId;
    data['trackName'] = trackName;
    data['artistName'] = artistName;
    data['collectionName'] = collectionName;
    data['previewUrl'] = previewUrl;
    data['artworkUrl100'] = artworkUrl100;
    data['releaseDate'] = releaseDate;
    data['trackTimeMillis'] = trackTimeMillis;
    data['country'] = country;
    data['primaryGenreName'] = primaryGenreName;
    return data;
  }
}
