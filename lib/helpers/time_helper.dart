import 'package:intl/intl.dart';

class TimeHelper {
  static String getReleaseDate(String date) {
    String releaseDate = '';

    final convertDate = DateTime.parse(date);

    releaseDate = DateFormat('yyyy').format(convertDate);

    return releaseDate;
  }

  static String formatCurrentDuration(Duration duration) {
    String twoDigits(int time) => time.toString().padLeft(2, '0');

    final hours = twoDigits(duration.inHours);
    final minutes = twoDigits(duration.inMinutes.remainder(60));
    final seconds = twoDigits(duration.inSeconds.remainder(60));

    return [if (duration.inHours > 0) hours, minutes, seconds].join(':');
  }
}
