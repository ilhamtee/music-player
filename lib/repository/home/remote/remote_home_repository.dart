import 'dart:convert';
import 'dart:io';

import 'package:audio_player/entities/music/response_song_model.dart';
import 'package:audio_player/helpers/constant_helper.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

class RemoteHomeRepository {
  final Dio dio = Dio();
  ResponseSongModel? responseSongModel;

  Future<ResponseSongModel?> getDefaultListSong() async {
    try {
      final response = await dio
          .get('${ConstantHelper.baseUrl}${ConstantHelper.endPointSearch}'
              '?term=tiara andini&entity=song');

      if (response.statusCode == HttpStatus.ok) {
        responseSongModel =
            ResponseSongModel.fromJson(json.decode(response.data));
      } else {
        responseSongModel = ResponseSongModel(
            listSong: [], status: false, statusCode: HttpStatus.badRequest);
      }

      return responseSongModel;
    } catch (e, stackTrace) {
      debugPrint(e.toString());
      debugPrint(stackTrace.toString());
      return responseSongModel;
    }
  }

  Future<ResponseSongModel?> searchSong({required String searchParam}) async {
    try {
      final response = await dio
          .get('${ConstantHelper.baseUrl}${ConstantHelper.endPointSearch}'
              '?term=$searchParam&entity=song');

      if (response.statusCode == HttpStatus.ok) {
        responseSongModel =
            ResponseSongModel.fromJson(json.decode(response.data));
      } else {
        responseSongModel = ResponseSongModel(
            listSong: [], status: false, statusCode: HttpStatus.badRequest);
      }

      return responseSongModel;
    } catch (e, stackTrace) {
      debugPrint(e.toString());
      debugPrint(stackTrace.toString());
      return responseSongModel;
    }
  }
}
