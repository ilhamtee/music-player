import 'package:audio_player/bloc/home/home_bloc.dart';
import 'package:audio_player/pages/home/home_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (context) => HomeBloc(),
        child: const MaterialApp(
          title: 'Music Player',
          debugShowCheckedModeBanner: false,
          home: HomePage(),
        ));
  }
}
