part of 'home_bloc.dart';

abstract class HomeEvent extends Equatable {}

class InitialHomeEvent extends HomeEvent {
  @override
  List<Object?> get props => [];
}

class SearchSongEvent extends HomeEvent {
  final String querySearch;

  SearchSongEvent({required this.querySearch});

  @override
  List<Object?> get props => [];
}

class HandlePaginationSongEvent extends HomeEvent {
  @override
  List<Object?> get props => [];
}
