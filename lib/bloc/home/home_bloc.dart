import 'dart:async';

import 'package:audio_player/entities/music/response_song_model.dart';
import 'package:audio_player/repository/home/remote/remote_home_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'home_event.dart';
part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final RemoteHomeRepository _remoteHomeRepository = RemoteHomeRepository();

  HomeBloc() : super(HomeState.initial()) {
    on<InitialHomeEvent>(_handleInitialHome);
    on<SearchSongEvent>(_handleSearchSong);
    on<HandlePaginationSongEvent>(_handlePaginationSong);
  }

  Future<void> _handleInitialHome(
      InitialHomeEvent event, Emitter<HomeState> emit) async {
    emit(state.copyWith(isLoading: true));
    List<Song> mainListSong = [];
    List<Song> filteredListSong = [];
    final resultGetSong = await _remoteHomeRepository.getDefaultListSong();

    if (resultGetSong != null) {
      if (resultGetSong.status!) {
        mainListSong = resultGetSong.listSong ?? [];
      }
    }

    // Set the length of the list song
    if (mainListSong.length > 15) {
      filteredListSong = mainListSong.getRange(0, 15).toList();
    } else {
      filteredListSong = mainListSong;
    }

    emit(state.copyWith(
        homeStateStatus: HomeStateStatus.initial,
        isLoading: false,
        mainListSong: mainListSong,
        filteredListSong: filteredListSong));
  }

  Future<void> _handleSearchSong(
      SearchSongEvent event, Emitter<HomeState> emit) async {
    emit(state.copyWith(isLoading: true));
    List<Song> mainListSong = [];
    List<Song> filteredListSong = [];
    final resultGetSong =
        await _remoteHomeRepository.searchSong(searchParam: event.querySearch);

    if (resultGetSong != null) {
      if (resultGetSong.status!) {
        mainListSong = resultGetSong.listSong ?? [];
      }
    }

    // Set the length of the list song
    if (mainListSong.length > 15) {
      filteredListSong = mainListSong.getRange(0, 15).toList();
    } else {
      filteredListSong = mainListSong;
    }

    emit(state.copyWith(
        homeStateStatus: HomeStateStatus.searchSong,
        isLoading: false,
        mainListSong: mainListSong,
        filteredListSong: filteredListSong));
  }

  Future<void> _handlePaginationSong(
      HandlePaginationSongEvent event, Emitter<HomeState> emit) async {
    List<Song> mainListSong = state.mainListSong;
    List<Song> updatedFilteredListSong = state.filteredListSong;

    if (mainListSong.length != updatedFilteredListSong.length) {
      emit(state.copyWith(isScrolling: true));
      int remainderLength =
          mainListSong.length - updatedFilteredListSong.length;
      if (remainderLength < 10) {
        // if the remaining songs are less than 10 then replace
        // with the main list song
        updatedFilteredListSong = mainListSong;
      } else {
        // add data by 10 songs
        updatedFilteredListSong = mainListSong
            .getRange(0, updatedFilteredListSong.length + 10)
            .toList();
      }
    }

    await Future.delayed(const Duration(milliseconds: 1500));

    emit(state.copyWith(
        isScrolling: false, filteredListSong: updatedFilteredListSong));
  }
}
