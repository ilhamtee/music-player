part of 'home_bloc.dart';

class HomeState extends Equatable {
  final HomeStateStatus homeStateStatus;
  final List<Song> mainListSong;
  final List<Song> filteredListSong;

  final bool isLoading;
  final bool isScrolling;

  const HomeState(
      {required this.homeStateStatus,
      required this.mainListSong,
      required this.filteredListSong,
      required this.isLoading,
      required this.isScrolling});

  factory HomeState.initial() => const HomeState(
      homeStateStatus: HomeStateStatus.initial,
      mainListSong: [],
      filteredListSong: [],
      isLoading: true,
      isScrolling: false);

  HomeState copyWith(
      {HomeStateStatus? homeStateStatus,
      List<Song>? mainListSong,
      List<Song>? filteredListSong,
      bool? isLoading,
      bool? isScrolling}) {
    return HomeState(
        homeStateStatus: homeStateStatus ?? this.homeStateStatus,
        mainListSong: mainListSong ?? this.mainListSong,
        filteredListSong: filteredListSong ?? this.filteredListSong,
        isLoading: isLoading ?? this.isLoading,
        isScrolling: isScrolling ?? this.isScrolling);
  }

  @override
  List<Object?> get props =>
      [mainListSong, filteredListSong, homeStateStatus, isLoading, isScrolling];
}

enum HomeStateStatus { initial, searchSong }
