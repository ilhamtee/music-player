part of 'audio_bloc.dart';

abstract class AudioEvent extends Equatable {
  const AudioEvent();
}

class InitialAudioEvent extends AudioEvent {
  final String urlSong;

  const InitialAudioEvent({required this.urlSong});

  @override
  List<Object?> get props => [urlSong];
}

class PlayAudioEvent extends AudioEvent {
  final String urlSong;

  const PlayAudioEvent({required this.urlSong});

  @override
  List<Object?> get props => [urlSong];
}

class PauseAudioEvent extends AudioEvent {
  @override
  List<Object?> get props => [];
}

class ResumeAudioEvent extends AudioEvent {
  @override
  List<Object?> get props => [];
}

class SetReleaseModeEvent extends AudioEvent {
  @override
  List<Object?> get props => [];
}

class AudioPlayerPositionChangedEvent extends AudioEvent {
  final Duration newDuration;

  const AudioPlayerPositionChangedEvent({required this.newDuration});

  @override
  List<Object?> get props => [newDuration];
}

class AudioPlayerDurationChangedEvent extends AudioEvent {
  final Duration newDuration;

  const AudioPlayerDurationChangedEvent({required this.newDuration});

  @override
  List<Object?> get props => [newDuration];
}

class SeekAudioEvent extends AudioEvent {
  final Duration newDuration;

  const SeekAudioEvent({required this.newDuration});

  @override
  List<Object?> get props => [newDuration];
}
