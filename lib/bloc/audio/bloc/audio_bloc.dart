import 'package:audioplayers/audioplayers.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'audio_event.dart';
part 'audio_state.dart';

class AudioBloc extends Bloc<AudioEvent, AudioState> {
  AudioPlayer audioPlayer = AudioPlayer();

  AudioBloc() : super(AudioState.initial()) {
    on<InitialAudioEvent>(_handleInitialAudio);
    on<PlayAudioEvent>(_handlePlayAudio);
    on<PauseAudioEvent>(_handlePauseAudio);
    on<ResumeAudioEvent>(_handleResumeAudio);
    on<AudioPlayerDurationChangedEvent>(_handleAudioDurationChanged);
    on<AudioPlayerPositionChangedEvent>(_handleAudioPositionChanged);
    on<SetReleaseModeEvent>(_handleReleaseMode);
    on<SeekAudioEvent>(_handleSeekAudio);
  }

  Future<void> _handleInitialAudio(
      InitialAudioEvent event, Emitter emit) async {
    int result = await audioPlayer.setUrl(event.urlSong);
    audioPlayer.setReleaseMode(ReleaseMode.STOP);
    int duration = (await audioPlayer.getDuration());
    Duration maxDuration = Duration(milliseconds: duration);
    emit(state.copyWith(maxDuration: maxDuration));
    

    if (result == 1) {
      // Listen audio state : PLAYING, PAUSE, STOP
      audioPlayer.onPlayerStateChanged.listen((state) {});

      // Listen to audio duration
      audioPlayer.onDurationChanged.listen((newDuration) {
        add(AudioPlayerDurationChangedEvent(newDuration: newDuration));
        
      });

      // Listen to audio position
      audioPlayer.onAudioPositionChanged.listen((newDuration) {
        add(AudioPlayerPositionChangedEvent(newDuration: newDuration));
        
      });

      audioPlayer.onPlayerCompletion.listen((event) {
        if (state.isRepeatSong == false) {
          add(PauseAudioEvent());
        }
      });

      emit(state);
    }
  }

  Future<void> _handlePlayAudio(PlayAudioEvent event, Emitter emit) async {
    await audioPlayer.play(event.urlSong);

    emit(state.copyWith(isPlaying: true));
  }

  Future<void> _handlePauseAudio(PauseAudioEvent event, Emitter emit) async {
    await audioPlayer.pause();

    emit(state.copyWith(isPlaying: false));
  }

  Future<void> _handleResumeAudio(ResumeAudioEvent event, Emitter emit) async {
    audioPlayer.resume();

    emit(state);
  }

  Future<void> _handleAudioPositionChanged(
      AudioPlayerPositionChangedEvent event, Emitter emit) async {
    emit(state.copyWith(currentDuration: event.newDuration));
  }

  Future<void> _handleAudioDurationChanged(
      AudioPlayerDurationChangedEvent event, Emitter emit) async {
    emit(state.copyWith(maxDuration: event.newDuration));
  }

  Future<void> _handleReleaseMode(
      SetReleaseModeEvent event, Emitter emit) async {
    bool repeatSong = state.isRepeatSong;

    if (!repeatSong) {
      audioPlayer.setReleaseMode(ReleaseMode.LOOP);

      emit(state.copyWith(isRepeatSong: true));
    } else {
      audioPlayer.setReleaseMode(ReleaseMode.STOP);

      emit(state.copyWith(isRepeatSong: false));
    }

    add(ResumeAudioEvent());
  }

  Future<void> _handleSeekAudio(SeekAudioEvent event, Emitter emit) async {
    audioPlayer.seek(event.newDuration);

    add(ResumeAudioEvent());

    emit(state);
  }

  @override
  Future<void> close() {
    audioPlayer.dispose();
    return super.close();
  }
}
