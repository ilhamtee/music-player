// ignore_for_file: public_member_api_docs, sort_constructors_first
part of 'audio_bloc.dart';

class AudioState extends Equatable {
  final bool isPlaying;
  final bool isRepeatSong;
  final bool isFavorite;

  final Duration maxDuration;
  final Duration currentDuration;

  const AudioState(
      {required this.isPlaying,
      required this.isRepeatSong,
      required this.isFavorite,
      required this.maxDuration,
      required this.currentDuration});

  factory AudioState.initial() => const AudioState(
      isFavorite: false,
      isPlaying: false,
      isRepeatSong: false,
      currentDuration: Duration.zero,
      maxDuration: Duration.zero);

  AudioState copyWith({
    bool? isPlaying,
    bool? isRepeatSong,
    bool? isFavorite,
    Duration? maxDuration,
    Duration? currentDuration,
  }) {
    return AudioState(
      isPlaying: isPlaying ?? this.isPlaying,
      isRepeatSong: isRepeatSong ?? this.isRepeatSong,
      isFavorite: isFavorite ?? this.isFavorite,
      maxDuration: maxDuration ?? this.maxDuration,
      currentDuration: currentDuration ?? this.currentDuration,
    );
  }

  @override
  List<Object> get props {
    return [
      isPlaying,
      isRepeatSong,
      isFavorite,
      maxDuration,
      currentDuration,
    ];
  }
}
