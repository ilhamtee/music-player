import 'package:audio_player/entities/music/response_song_model.dart';
import 'package:audio_player/helpers/time_helper.dart';
import 'package:audio_player/pages/audio_player/audio_player_page.dart';
import 'package:flutter/material.dart';

class SongItem extends StatelessWidget {
  final Song song;
  const SongItem({super.key, required this.song});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => AudioPlayerPage(song: song)));
      },
      leading: Image.network(
        song.artworkUrl100 ?? '',
        fit: BoxFit.fill,
        height: 80,
        width: 57,
      ),
      title: Text(
        song.trackName ?? '',
        style: const TextStyle(
            color: Colors.white, fontSize: 17, fontWeight: FontWeight.bold),
      ),
      subtitle: Row(
        children: [
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 2, vertical: 2),
            decoration: const BoxDecoration(
                color: Colors.grey,
                borderRadius: BorderRadius.all(Radius.circular(2))),
            child: Text(
              TimeHelper.getReleaseDate(song.releaseDate ?? ''),
              style: const TextStyle(color: Colors.black, fontSize: 10),
            ),
          ),
          const SizedBox(
            width: 10,
          ),
          Flexible(
            child: Text(
              song.artistName ?? '',
              style: const TextStyle(color: Colors.white, fontSize: 13),
            ),
          ),
        ],
      ),
      trailing: const Icon(
        Icons.play_arrow,
        color: Colors.yellow,
        size: 35,
      ),
    );
  }
}
