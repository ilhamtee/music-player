import 'package:audio_player/bloc/home/home_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

// ignore: must_be_immutable
class SearchSongWidget extends StatelessWidget {
  SearchSongWidget({super.key});

  final TextEditingController searchController = TextEditingController();

  HomeBloc? _homeBloc;
  @override
  Widget build(BuildContext context) {
    _homeBloc ??= context.read<HomeBloc>();
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(5))),
      child: Row(
        children: [
          const Icon(Icons.search),
          const SizedBox(
            width: 5,
          ),
          Expanded(
              child: TextField(
            controller: searchController,
            cursorColor: Colors.yellow,
            style: const TextStyle(color: Colors.black, fontSize: 18),
            maxLines: 1,
            onSubmitted: (value) {
              if (value.isNotEmpty) {
                _homeBloc?.add(SearchSongEvent(querySearch: value));
              } else {
                _homeBloc?.add(InitialHomeEvent());
              }
            },
            decoration: const InputDecoration(
                border: InputBorder.none,
                hintText: "What song do you want to listen to?",
                hintStyle: TextStyle(fontSize: 15),
                contentPadding: EdgeInsets.symmetric(horizontal: 5)),
          ))
        ],
      ),
    );
  }
}
