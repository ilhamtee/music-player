import 'package:audio_player/bloc/home/home_bloc.dart';
import 'package:audio_player/entities/music/response_song_model.dart';
import 'package:audio_player/pages/home/widgets/search_song_widget.dart';
import 'package:audio_player/pages/home/widgets/song_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final TextEditingController _searchController = TextEditingController();
  final ScrollController _scrollController = ScrollController();

  late HomeBloc _homeBloc;

  @override
  void initState() {
    super.initState();
    _homeBloc = context.read<HomeBloc>();
    _homeBloc.add(InitialHomeEvent());

    // if (_scrollController.hasClients) {
    //   _scrollController.addListener(() {
    //     if (_scrollController.position.pixels ==
    //         _scrollController.position.maxScrollExtent) {
    //       print('HAHAHHAA');
    //     }
    //   });
    // }
  }

  @override
  void dispose() {
    super.dispose();
    _homeBloc.close();
    _searchController.dispose();
    _scrollController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocConsumer<HomeBloc, HomeState>(
        bloc: _homeBloc,
        listener: (context, state) {
          switch (state.homeStateStatus) {
            case HomeStateStatus.initial:
              // For now nothing to do
              break;
            case HomeStateStatus.searchSong:
              // For now nothing to do
              break;
          }
        },
        builder: (context, state) {
          return SafeArea(
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: const <Widget>[
                      Icon(
                        Icons.queue_music,
                        color: Colors.yellow,
                        size: 30,
                      ),
                      Text(
                        'Music Player',
                        style: TextStyle(
                            color: Colors.yellow,
                            fontSize: 25,
                            fontWeight: FontWeight.w500),
                      ),
                    ],
                  ),
                  const SizedBox(height: 30),
                  SearchSongWidget(),
                  const SizedBox(height: 50),
                  state.isLoading
                      ? const Center(
                          child:
                              CircularProgressIndicator(color: Colors.yellow))
                      : !state.isLoading && state.filteredListSong.isNotEmpty
                          ? Expanded(
                              child: NotificationListener<ScrollNotification>(
                                onNotification: (scrollNotification) {
                                  if (scrollNotification
                                      is ScrollEndNotification) {
                                    final metrics = scrollNotification.metrics;
                                    if (metrics.atEdge) {
                                      bool isTop = metrics.pixels == 0;
                                      if (!isTop) {
                                        _homeBloc
                                            .add(HandlePaginationSongEvent());
                                      }
                                    }
                                  }

                                  return true;
                                },
                                child: ListView.builder(
                                    controller: _scrollController,
                                    shrinkWrap: true,
                                    itemCount: state.filteredListSong.length,
                                    itemBuilder: (context, index) {
                                      Song data = state.filteredListSong[index];
                                      return SongItem(song: data);
                                    }),
                              ),
                            )
                          : const Center(
                              child: Text(
                                'The song you are looking for is not found',
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                  state.isScrolling
                      ? Center(
                          child: Container(
                            width: 20,
                            height: 20,
                            margin: const EdgeInsets.symmetric(vertical: 10),
                            child: const CircularProgressIndicator(
                              color: Colors.yellow,
                            ),
                          ),
                        )
                      : const SizedBox()
                ],
              ),
            ),
          );
        },
      ),
      backgroundColor: Colors.black,
    );
  }
}
