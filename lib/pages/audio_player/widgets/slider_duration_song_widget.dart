import 'package:audio_player/bloc/audio/bloc/audio_bloc.dart';
import 'package:audio_player/helpers/time_helper.dart';
import 'package:flutter/material.dart';

class SliderDurationSongWidget extends StatelessWidget {
  const SliderDurationSongWidget({
    Key? key,
    required this.maxDuration,
    required this.currentDuration,
    required this.audioBloc,
  }) : super(key: key);

  final Duration maxDuration;
  final Duration currentDuration;
  final AudioBloc audioBloc;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: Column(
        children: [
          Slider(
            min: 0,
            max: maxDuration.inSeconds.toDouble(),
            value: currentDuration.inSeconds.toDouble(),
            activeColor: Colors.yellow,
            inactiveColor: Colors.yellow.withOpacity(0.5),
            onChanged: (value) async {
              final positionDuration = Duration(seconds: value.toInt());
              audioBloc.add(SeekAudioEvent(newDuration: positionDuration));
            },
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  TimeHelper.formatCurrentDuration(currentDuration),
                  style: const TextStyle(color: Colors.white),
                ),
                Text(
                  TimeHelper.formatCurrentDuration(maxDuration),
                  style: const TextStyle(color: Colors.white),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
