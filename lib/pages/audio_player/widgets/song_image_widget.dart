import 'package:flutter/material.dart';

class SongImageWidget extends StatelessWidget {
  const SongImageWidget({
    Key? key,
    required this.imageUrl,
  }) : super(key: key);

  final String imageUrl;

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: const BorderRadius.all(Radius.circular(10)),
      child: Image.network(
        imageUrl,
        fit: BoxFit.fill,
        height: 150,
        width: 140,
      ),
    );
  }
}
