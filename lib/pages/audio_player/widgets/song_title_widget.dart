import 'package:flutter/material.dart';

class SongTitleWidget extends StatelessWidget {
  const SongTitleWidget({
    Key? key,
    required this.titleSong,
    required this.artistName,
    required this.genre,
  }) : super(key: key);

  final String titleSong;
  final String artistName;
  final String genre;

  @override
  Widget build(BuildContext context) {
    return ListTile(
        title: Text(
          titleSong,
          style: const TextStyle(
              color: Colors.white, fontSize: 20, fontWeight: FontWeight.bold),
        ),
        subtitle: Text(
          artistName,
          style: const TextStyle(
            color: Colors.white,
            fontSize: 15,
          ),
        ),
        trailing: Container(
          padding: const EdgeInsets.symmetric(horizontal: 5),
          decoration: const BoxDecoration(
              color: Colors.grey,
              borderRadius: BorderRadius.all(Radius.circular(3))),
          child: Text(
            genre.toUpperCase(),
            style: const TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w500,
                fontSize: 10,
                fontStyle: FontStyle.italic),
          ),
        ));
  }
}
