import 'package:audio_player/bloc/audio/bloc/audio_bloc.dart';
import 'package:audio_player/entities/music/response_song_model.dart';
import 'package:audio_player/pages/audio_player/widgets/slider_duration_song_widget.dart';
import 'package:audio_player/pages/audio_player/widgets/song_image_widget.dart';
import 'package:audio_player/pages/audio_player/widgets/song_title_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AudioPlayerPage extends StatefulWidget {
  final Song song;

  const AudioPlayerPage({Key? key, required this.song}) : super(key: key);

  @override
  State<AudioPlayerPage> createState() => _AudioPlayerPageState();
}

class _AudioPlayerPageState extends State<AudioPlayerPage> {
  bool isFavorite = false;

  final AudioBloc audioBloc = AudioBloc();

  @override
  void initState() {
    super.initState();
    audioBloc.add(InitialAudioEvent(urlSong: widget.song.previewUrl!));
  }

  @override
  void dispose() {
    audioBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.black,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          title: const Text(
            'Enjoy the music',
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
          ),
        ),
        body: SafeArea(
          child: Center(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 50),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SongImageWidget(imageUrl: widget.song.artworkUrl100 ?? ''),

                  const SizedBox(height: 40),
                  SongTitleWidget(
                    artistName: widget.song.artistName ?? '',
                    genre: widget.song.primaryGenreName ?? '',
                    titleSong: widget.song.trackName ?? '',
                  ),

                  const SizedBox(height: 50),
                  BlocBuilder<AudioBloc, AudioState>(
                    bloc: audioBloc,
                    builder: (context, state) {
                      return SliderDurationSongWidget(
                          maxDuration: state.maxDuration,
                          currentDuration: state.currentDuration,
                          audioBloc: audioBloc);
                    },
                  ),

                  const SizedBox(height: 50),
                  // Toggle play button and repeat button
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      BlocSelector<AudioBloc, AudioState, bool>(
                        bloc: audioBloc,
                        selector: (state) => state.isRepeatSong,
                        builder: (context, state) {
                          return Expanded(
                            child: Container(
                              alignment: Alignment.centerLeft,
                              padding: const EdgeInsets.only(left: 10),
                              child: GestureDetector(
                                onTap: () async {
                                  audioBloc.add(SetReleaseModeEvent());
                                },
                                child: Icon(
                                  state
                                      ? Icons.repeat_one_outlined
                                      : Icons.repeat_outlined,
                                  size: 40,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          );
                        },
                      ),
                      BlocBuilder<AudioBloc, AudioState>(
                        bloc: audioBloc,
                        builder: (context, state) {
                          return Expanded(
                            child: SizedBox(
                              child: GestureDetector(
                                onTap: () async {
                                  if (state.isPlaying) {
                                    audioBloc.add(PauseAudioEvent());
                                  } else {
                                    audioBloc.add(PlayAudioEvent(
                                        urlSong: widget.song.previewUrl!));
                                  }
                                },
                                child: Container(
                                  width: 50,
                                  height: 50,
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: state.maxDuration != Duration.zero
                                          ? Colors.yellow
                                          : Colors.grey),
                                  child: Icon(
                                    state.isPlaying
                                        ? Icons.pause
                                        : Icons.play_arrow,
                                    size: 40,
                                    color: state.maxDuration != Duration.zero
                                        ? Colors.black
                                        : Colors.grey[800],
                                  ),
                                ),
                              ),
                            ),
                          );
                        },
                      ),
                      const Expanded(child: SizedBox())
                    ],
                  ),
                  const SizedBox(height: 50)
                ],
              ),
            ),
          ),
        ));
  }
}
